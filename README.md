# add-samesite-cookie-attribute Atlassian Plugin

A super-simple plugin that adds `SameSite` cookie attribute (and `Secure` if `None` is chosen as the value and it's a secure request) to chosen cookies. 

Currently, it adds `SameSite=None; Secure` to all known Atlassian Server/DC session cookies (if they have the default name of `JSESSIONID` or `BITBUCKETSESSIONID`). This is useful if you require certain SSO features or iframing of your instance or want to fix some avatar pictures.

Simply install from the [downloads section](https://bitbucket.org/resolutiongmbh/add-samesite-cookie-attribute-plugin/downloads/) into any Atlassian host app and enjoy.

NOTE 1: If Atlassian ever decides to add SameSite handling to its cookies, this plugin will just not touch those cookies anymore.

NOTE 2: If you run a Data Center installation, please make sure that your load balancer also adds the `SameSite=None; Secure` attributes to its own session stickiness cookie!!

NOTE 3: Sometimes, it seems like the cookie attribute is only added after a successful login or after a restart. No idea why.

This code is largely based on code by the Shibboleth project: https://wiki.shibboleth.net/confluence/display/DEV/IdP+SameSite+Filter+Implementation (licensed under Apache 2)

## Modifying and Building

By default, this plugin will cause the session cookies `JSESSIONID` and `BITBUCKETSESSIONID` to get the attribute `SameSite=None; Secure`. You can modify this by looking for the constructor in the `SameSiteCookieAttributeFilter` class (around line 147-ish) and modifying the map.

To build this plugin again afterwards, install the [Atlassian SDK](https://developer.atlassian.com/server/framework/atlassian-sdk/downloads/) on your machine, then navigate a terminal to this repository's directory and run

```bash
atlas-mvn clean install
```

The resulting artifact `jar` file will be located in the `target` directory.

## If you like this app...

...check out our [website](https://resolution.de) or our listings on the [Atlassian Marketplace](https://marketplace.atlassian.com/vendors/1210947/resolution-reichert-network-solutions-gmbh) :)
