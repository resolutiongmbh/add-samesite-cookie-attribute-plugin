package de.resolution.addsamesitecookieattribute;
/*
 * Licensed to the University Corporation for Advanced Internet Development,
 * Inc. (UCAID) under one or more contributor license agreements.  See the
 * NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The UCAID licenses this file to You under the Apache
 * License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * With modifications by resolution Reichert Network Solutions GmbH
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpCookie;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of an HTTP servlet {@link Filter} which conditionally adds the SameSite attribute to cookies.
 *
 * <p>Affected cookies are configured and placed into a Map of cookie name to same-site attribute value.</p>
 *
 * <p>Cookies with an existing same-site cookie flag, or those not present in the {@code sameSiteCookies} Map, are
 * left unaltered - copied back into the response without modification.
 *
 * <p>A single cookie can only have at most one same-site value set. Attempts in the configuration to
 * give more than one same-site value to a cookie are caught during argument injection and throw an {@link
 * IllegalArgumentException}.</p>
 */
public class SameSiteCookieAttributeFilter implements Filter {

    public static final String SET_COOKIE = "Set-Cookie";

    private static final String ALREADY_FILTERED = SameSiteCookieAttributeFilter.class.getName() + "_already_filtered";

    /**
     * Class logger.
     */
    @Nonnull
    private final Logger log = LoggerFactory.getLogger(SameSiteCookieAttributeFilter.class);

    /**
     * The name of the same-site cookie attribute.
     */
    private static final String SAMESITE_ATTRIBUTE_NAME = "SameSite";

    /**
     * The name of the secure cookie attribute.
     */
    private static final String SECURE_ATTRIBUTE_NAME = "Secure";

    /**
     * The allowed same-site cookie attribute values.
     */
    public enum SameSiteValue {

        /**
         * Send the cookie for 'same-site' requests only.
         */
        Strict("Strict", false),
        /**
         * Send the cookie for 'same-site' requests along with 'cross-site' top level navigations using safe HTTP
         * methods (GET, HEAD, OPTIONS, and TRACE).
         */
        Lax("Lax", false),
        /**
         * Send the cookie for 'same-site' and 'cross-site' requests.
         */
        None("None", true);

        /**
         * The same-site attribute value.
         */
        private final String value;

        /**
         * Whether the chosen SameSite value requires Secure to be added as well.
         */
        private final boolean forceSecure;

        /**
         * Constructor.
         *
         * @param attrValue the same-site attribute value.
         */
        private SameSiteValue(final String attrValue, final boolean forceSecure) {
            value = attrValue;
            this.forceSecure = forceSecure;
        }

        /**
         * Get the same-site attribute value.
         *
         * @return Returns the value.
         */
        public String getValue() {
            return value;
        }

        public boolean isForceSecure() {
            return forceSecure;
        }
    }

    /**
     * Map of cookie name to same-site attribute value.
     */
    @Nonnull
    private Map<String, SameSiteValue> sameSiteCookies = Collections.emptyMap();


    /**
     * Constructor.
     */
    public SameSiteCookieAttributeFilter() {
        HashMap<SameSiteValue, List<String>> cookieSettings = new HashMap<>();

        cookieSettings.put(SameSiteValue.None, Arrays.asList("JSESSIONID", "BITBUCKETSESSIONID"));

        setSameSiteCookies(cookieSettings);
    }

    /**
     * Set the names of cookies to add the same-site attribute to.
     *
     * <p>The argument map is flattened to remove the nested collection. The argument map allows duplicate
     * cookie names to appear in order to detect configuration errors which would otherwise not be found during argument
     * injection e.g. trying to set a session identifier cookie as both SameSite=Strict and SameSite=None. Instead,
     * duplicates are detected here, throwing a terminating {@link IllegalArgumentException} if found.</p>
     *
     * @param map the map of same-site attribute values to cookie names.
     */
    public void setSameSiteCookies(@Nullable final Map<SameSiteValue, List<String>> map) {
        if (map != null) {
            sameSiteCookies = new HashMap<>(4);
            for (final Map.Entry<SameSiteValue, List<String>> entry : map.entrySet()) {

                for (final String cookieName : entry.getValue()) {
                    if (sameSiteCookies.get(cookieName) != null) {
                        log.error("Duplicate cookie name [{}] found in SameSite cookie map, "
                                + "please check configuration.", cookieName);
                        throw new IllegalArgumentException("Duplicate cookie name found in SameSite cookie map");
                    }
                    if (cookieName != null && !cookieName.trim().isEmpty()) {
                        sameSiteCookies.put(cookieName, entry.getKey());
                    }
                }
            }
        } else {
            sameSiteCookies = Collections.emptyMap();
        }
        log.debug("Set SameSite cookie config to {}", sameSiteCookies.toString());
    }

    /**
     * {@inheritDoc}
     */
    public void init(@Nonnull final FilterConfig filterConfig) throws ServletException {
        log.info("Initializing SameSite Cookie Attribute Filter");
    }

    /**
     * {@inheritDoc}
     */
    public void destroy() {
    }

    /**
     * {@inheritDoc}
     */
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        if (request.getAttribute(ALREADY_FILTERED) != null) {
            chain.doFilter(request, response);
            return;
        }
        request.setAttribute(ALREADY_FILTERED, "yes");


        if (!(request instanceof HttpServletRequest)) {
            throw new ServletException("Request is not an instance of HttpServletRequest");
        }

        if (!(response instanceof HttpServletResponse)) {
            throw new ServletException("Response is not an instance of HttpServletResponse");
        }

        chain.doFilter(request, new SameSiteResponseProxy((HttpServletResponse) response, "https".equalsIgnoreCase(request.getScheme())));

    }

    /**
     * An implementation of the {@link HttpServletResponse} which adds the same-site flag to {@literal Set-Cookie}
     * headers for the set of configured cookies.
     */
    private class SameSiteResponseProxy extends HttpServletResponseWrapper {

        /**
         * The response.
         */
        @Nonnull
        private final HttpServletResponse response;

        /**
         * Whether the request is secure, allowing for SameSite=None; Secure additions.
         */
        private final boolean isRequestSecure;

        /**
         * Constructor.
         *
         * @param resp the response to delegate to
         */
        public SameSiteResponseProxy(@Nonnull final HttpServletResponse resp, final boolean isRequestSecure) {
            super(resp);
            response = resp;
            this.isRequestSecure = isRequestSecure;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void sendError(final int sc) throws IOException {
            log.trace("sendError(int) called");
            appendSameSite();
            super.sendError(sc);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public PrintWriter getWriter() throws IOException {
            log.trace("getWriter() called");
            appendSameSite();
            return super.getWriter();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void sendError(final int sc, final String msg) throws IOException {
            log.trace("sendError(int, String) called");
            appendSameSite();
            super.sendError(sc, msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void sendRedirect(final String location) throws IOException {
            log.trace("sendRedirect(String) called");
            appendSameSite();
            super.sendRedirect(location);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            log.trace("getOutputStream() called");
            appendSameSite();
            return super.getOutputStream();
        }

        /**
         * Add the SameSite attribute to those cookies configured in the {@code sameSiteCookies} map iff they do not
         * already contain the same-site flag. All other cookies are copied over to the response without modification.
         */
        private void appendSameSite() {

            final Collection<String> cookieHeaders = response.getHeaders(SET_COOKIE);

            // bail early if there's nothing to set
            if (sameSiteCookies.isEmpty()) {
                return;
            }

            boolean firstHeader = true;
            for (final String cookieHeader : cookieHeaders) {

                if (cookieHeader == null || cookieHeader.trim().isEmpty()) {
                    continue;
                }

                List<HttpCookie> parsedCookies = null;
                try {
                    //this parser only parses name and value, we only need the name.
                    parsedCookies = HttpCookie.parse(cookieHeader);
                } catch (final IllegalArgumentException e) {
                    //should not get here
                    log.trace("Cookie header [{}] violates the cookie specification and will be ignored", cookieHeader);
                }

                if (parsedCookies == null || parsedCookies.size() != 1) {
                    //should be one cookie
                    continue;
                }

                final String cookieName = parsedCookies.get(0).getName();
                final SameSiteValue sameSiteValue = sameSiteCookies.get(cookieName);

                // only add the header(s) on a secure request if the context is secure as well
                boolean alterCookie = sameSiteValue != null;
                if (alterCookie && sameSiteValue.isForceSecure() && !isRequestSecure) {
                    alterCookie = false;
                    log.debug("Not adding 'SameSite={}' to {} as it requires the cookie to be set as Secure as well, which would require the request to be served via HTTPS, which it isn't (or the configuration is messed up)", sameSiteValue, cookieName);
                }

                String cookieHeaderToSet = cookieHeader;
                if (alterCookie) {
                    log.debug("Attempting to add 'SameSite={}' to cookie {}", sameSiteValue, cookieName);
                    cookieHeaderToSet = appendSameSiteAttribute(cookieHeader, sameSiteValue.getValue(), sameSiteValue.isForceSecure());
                }

                // Put the header back in the request again
                if (firstHeader) {
                    log.trace("Setting {}: {}", SET_COOKIE, cookieHeaderToSet);
                    response.setHeader(SET_COOKIE, cookieHeaderToSet);
                } else {
                    log.trace("Adding {}: {}", SET_COOKIE, cookieHeaderToSet);
                    response.addHeader(SET_COOKIE, cookieHeaderToSet);
                }
                firstHeader = false;

            }
        }

        /**
         * Append the SameSite cookie attribute with the specified samesite-value to the {@code cookieHeader} iff it
         * does not already have one set.
         *
         * @param cookieHeader  the cookie header value.
         * @param sameSiteValue the SameSite attribute value e.g. None, Lax, or Strict.
         * @param forceSecure   Whether to force secure being added
         */
        private String appendSameSiteAttribute(@Nonnull final String cookieHeader, @Nonnull final String sameSiteValue,
                                               final boolean forceSecure) {

            String sameSiteSetCookieValue = cookieHeader;

            //only add if does not already exist, else leave
            if (!cookieHeader.contains(SAMESITE_ATTRIBUTE_NAME)) {
                sameSiteSetCookieValue = String.format("%s; %s", sameSiteSetCookieValue, SAMESITE_ATTRIBUTE_NAME + "=" + sameSiteValue);
            } else {
                log.debug("Not adding SameSite cookie attribute as it already has one!");
            }

            // add secure if forced and not already present
            if (forceSecure && !cookieHeader.contains(SECURE_ATTRIBUTE_NAME)) {
                log.trace("Also adding 'Secure' attribute");
                sameSiteSetCookieValue = String.format("%s; %s", sameSiteSetCookieValue, SECURE_ATTRIBUTE_NAME);
            } else {
                log.debug("Not adding Secure cookie attribute as it already has one!");
            }

            return sameSiteSetCookieValue;
        }

    }

}
